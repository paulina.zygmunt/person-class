#include <iostream>
#include "Person.h"
using namespace std;

int main(){
    Person student1("Jan", "Kowalski");
    cout << "student1: ";
    student1.wypisz();

    Person student2("Anna", "Zielinska");
    cout << endl << "student2: ";
    student2.wypisz();

    Person student3(student1);
    cout << endl << "student3: ";
    student3.wypisz();
    cout << " - created with a copy constructor from student1";

    student3.przypisz("Filip", "Nowak");
    cout << endl << "student3: ";
    student3.wypisz();
    cout << " - changed with an assigning function";

    Person student4;
    cout << endl << "student4: ";
    student4.wypisz();
    cout << " - created with a copy constructor without parameters (default values)";

    student4 = student4;
    cout << endl << "student3: ";
    student4.wypisz();
    cout << " - changed with an overloaded assignment operator";
    cout << endl;

    return 0;
}