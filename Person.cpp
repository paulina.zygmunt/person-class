#include <iostream>
#include "Person.h"

using namespace std;

Person::Person(string n, string s){
    name = n;
    surname = s;
}

Person::Person(Person & c){  //konstruktor kopiujacy
    name = c.name;
    surname = c.surname;
}

void Person::przypisz(string n, string s){
    name = n;
    surname = s;
}

void Person::wypisz(){
    cout << name << " " << surname;
}