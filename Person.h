#ifndef Person_H_
#define Person_H_

#include <iostream>
using std::string;

class Person{
    string name;
    string surname;

public:
    Person(string = "Name", string = "Surname"); //copy constructor
    Person(Person&);
    Person& operator=(const Person& per){  //overloaded assigning operator
        string tmp = per.name;
        name = per.surname;
        surname = tmp;
    };
    void przypisz(string, string);
    void wypisz();
};

#endif