This class hold two parameters: first and last name and has:
- copy constructor
- two-parameter constructor
- private fields: name and surname
- assignment operator
- function that prints name and surname
